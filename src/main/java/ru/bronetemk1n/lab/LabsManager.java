package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Scanner;

public class LabsManager {

    private int selectedLab = -1;

    public void start() {
        EventBus.getDefault().register(this);
        Scanner scanner = new Scanner(System.in);
        AbstractLab labRunnable;
        while(selectedLab != -2) {
            System.out.print("Enter the lab number: ");
            selectedLab = scanner.nextInt();
            switch (selectedLab) {
                case 1: {
                    labRunnable = new Lab1();
                    break;
                }
                case 2: {
                    labRunnable = new Lab2();
                    break;
                }
                case 3: {
                    labRunnable = new Lab3();
                    break;
                }
                default: {
                    labRunnable = AbstractLab.EMPTY;
                }
            }
            labRunnable.run();
        }
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onLabOutput(String content) {
        System.out.println(content);
    }

}
