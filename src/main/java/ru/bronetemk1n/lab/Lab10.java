package ru.bronetemk1n.lab;

import ru.bronetemk1n.lab.gui.AbstractLabGui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Lab10 extends AbstractLabGui {

    private JButton form1Button, form2Button;
    private JFrame form1, form2;

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container container = new Container();
        container.setLayout(new FlowLayout());

        form1 = new JFrame();
        configureFrame(form1, "This is form 1", "This is form 1 content");
        form2 = new JFrame();
        configureFrame(form2, "This is form 2", "This is form 2 content");

        form1Button = new JButton("Open");
        form1Button.addActionListener(this::actionForm1);
        form2Button = new JButton("Open");
        form2Button.addActionListener(this::actionForm2);
        container.add(form1Button);
        container.add(form2Button);
        return container;
    }

    @Override
    public String onExecute(int taskNumber) {
        return "";
    }

    @Override
    protected int getTaskCount() {
        return 1;
    }

    @Override
    protected void executeTask(int taskIndex) {
    }

    private void actionForm1(ActionEvent e) {
        form1.setVisible(!form1.isVisible());
        if(form1.isVisible())
            form1Button.setText("Close");
        else
            form1Button.setText("Open");
    }

    private void actionForm2(ActionEvent e) {
        form2.setVisible(!form2.isVisible());
        if(form2.isVisible())
            form2Button.setText("Close");
        else
            form2Button.setText("Open");
    }

    private void configureFrame(JFrame frame, String title, String content) {
        frame.setTitle(title);
        frame.setBounds(200, 100, 240, 240);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new JLabel(content));
    }
}
