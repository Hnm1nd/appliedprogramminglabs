package ru.bronetemk1n.lab;

import ru.bronetemk1n.lab.gui.AbstractLabGui;

import java.awt.*;

public class Lab3 extends AbstractLabGui {

    @Override
    protected int getTaskCount() {
        return 2;
    }

    @Override
    protected void executeTask(int taskIndex) {
        switch (taskIndex) {
            case 1: {
                Double[] srcValues = readValues(2);
                sendResult(String.valueOf(task1execute(srcValues[0], srcValues[1])));
                break;
            }
            case 2: {
                Double[] srcValues = readValues(1);
                sendResult(String.valueOf(task2execute(srcValues[0])));
                break;
            }
        }
    }

    private boolean task1execute(double arg1, double arg2) {
        return arg1 % arg2 == 0;
    }

    private double task2execute(double arg1) {
        double result;
        if (arg1 > 1 && arg1 < 2) {
            result = Math.cos(arg1) * Math.cos(arg1);
        } else {
            result = Math.sin(arg1)* Math.sin(arg1) + 1;
        }
        return result;
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container container = new Container();
        switch (taskNumber) {
            case 1: {
                buildTaskContainerWithInputs(container, new String[]{"P: ", "Q: "});
                break;
            }
            case 2: {
                buildTaskContainerWithInputs(container, new String[]{"X: "});
                break;
            }
        }
        return container;
    }

    @Override
    public String onExecute(int taskNumber) {
        switch (taskNumber) {
            case 1: {
                return String.valueOf(task1execute(getD(0), getD(1)));
            }
            case 2: {
                return String.valueOf(task2execute(getD(0)));
            }
        }
        return "";
    }
}
