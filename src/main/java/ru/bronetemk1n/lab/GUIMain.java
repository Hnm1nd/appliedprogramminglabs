package ru.bronetemk1n.lab;

import ru.bronetemk1n.lab.gui.AbstractLabGui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class GUIMain extends JFrame {

    private AbstractLabGui curLab;
    private Container taskSwitcherContainer,
            labContainer;
    private JComboBox<String> labSwitcher;
    private ButtonGroup taskSwitcherGroup;
    private JLabel resultLabel;

    public GUIMain() {
        this.setBounds(100, 100, 640, 480);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buildGui(this.getContentPane(), 1);
    }

    public static void main(String[] args) {
        GUIMain gui = new GUIMain();
        gui.setVisible(true);
    }

    private GridBagConstraints configureConstraints(GridBagConstraints constraints, int gridX, int gridY, double weightX, double weightY) {
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = gridX;
        constraints.gridy = gridY;
        constraints.weightx = weightX;
        constraints.weighty = weightY;
        constraints.insets = new Insets(8, 8, 8, 8);
        return constraints;
    }

    protected Container buildTaskSwitcher(int taskCount) {
        Container result = new Container();
        result.setLayout(new FlowLayout());
        taskSwitcherGroup = new ButtonGroup();
        for (int i = 1; i <= taskCount; i++) {
            JRadioButton button = new JRadioButton("Task " + i);
            button.setActionCommand(String.valueOf(i));
            button.addActionListener(this::onTaskSwitch);
            if (i == 1) button.setSelected(true);
            taskSwitcherGroup.add(button);
            result.add(button);
        }
        return result;
    }

    protected Container buildLabSwitcher(int labCount) {
        Container result = new Container();
        result.setLayout(new FlowLayout());

        String[] items = new String[labCount];
        for (int i = 1; i <= labCount; i++) {
            items[i - 1] = "Lab " + i;
        }
        labSwitcher = new JComboBox<>(items);
        labSwitcher.addActionListener(this::onLabSwitch);
        result.add(labSwitcher);
        return result;
    }

    protected int getTaskIndex() {
        if (taskSwitcherGroup != null) {
            ButtonModel model = taskSwitcherGroup.getSelection();
            if (model != null) {
                return Integer.valueOf(model.getActionCommand());
            } else return -1;
        } else return -2;
    }

    protected void onTaskSwitch(ActionEvent e) {
        labContainer.removeAll();
        labContainer.add(curLab.onContainerBuild(getTaskIndex()));
        labContainer.revalidate();
        resultLabel.setText("There will be the result of execution");
    }

    protected void onLabSwitch(ActionEvent e) {
        updateGui(((JComboBox<String>) e.getSource()).getSelectedIndex() + 1);
    }

    private void buildGui(Container container, int labIndex) {
        container.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        setTitle("APLab");
        curLab = getLab(labIndex);
        taskSwitcherContainer = new Container();
        taskSwitcherContainer.setLayout(new FlowLayout());
        taskSwitcherContainer.add(buildTaskSwitcher(curLab.getTaskCount()));
        labContainer = new Container();
        labContainer.setLayout(new FlowLayout());
        labContainer.add(curLab.onContainerBuild(getTaskIndex()));
        JButton executeButton = new JButton("Execute");
        executeButton.addActionListener(this::onExecuteTask);
        resultLabel = new JLabel("There will be the result of execution");
        container.add(taskSwitcherContainer, configureConstraints(constraints, 0, 1, 1, 1));
        container.add(buildLabSwitcher(12), configureConstraints(constraints, 1, 1, 1, 1));
        container.add(labContainer, configureConstraints(constraints, 0, 2, 1, 8));
        container.add(executeButton, configureConstraints(constraints, 0, 3, 0.5, 1));
        container.add(resultLabel, configureConstraints(constraints, 1, 3, 2, 1));
    }

    private void updateGui(int labIndex) {
        curLab = getLab(labIndex);
        taskSwitcherContainer.removeAll();
        taskSwitcherContainer.revalidate();
        taskSwitcherContainer.add(buildTaskSwitcher(curLab.getTaskCount()));
        labContainer.removeAll();
        labContainer.add(curLab.onContainerBuild(1));
        labContainer.revalidate();
        resultLabel.setText("There will be the result of execution");
    }

    private void onExecuteTask(ActionEvent e) {
        String result;
        try {
            result = "Result: " + curLab.onExecute(getTaskIndex());
        } catch (Throwable t) {
            result = "Error";
            t.printStackTrace();
        }
        resultLabel.setText(result);
    }

    private AbstractLabGui getLab(int labIndex) {
        switch (labIndex) {
            case 1: {
                return new Lab1();
            }
            case 2: {
                return new Lab2();
            }
            case 3: {
                return new Lab3();
            }
            case 4: {
                return new Lab4();
            }
            case 5: {
                return new Lab5();
            }
            case 6: {
                return new Lab6();
            }
            case 7: {
                return new Lab7();
            }
            case 8: {
                return new Lab8();
            }
            case 9: {
                return new Lab9();
            }
            case 10: {
                return new Lab10();
            }
            case 12: {
                return new Lab12();
            }
        }
        return AbstractLabGui.EMPTY;
    }

}
