package ru.bronetemk1n.lab;

import ru.bronetemk1n.lab.gui.AbstractLabGui;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class Lab12 extends AbstractLabGui {

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container result = new Container();
        buildTaskContainerWithInputs(result, "Path: ");
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        String path = inputs[0].getText();
        switch (taskNumber) {
            case 1: {
                return task1execute(path);
            }
            case 2: {
                return task2execute(path);
            }
            case 3: {
                return task3execute(path);
            }
        }
        return "Finished? Seems like that";
    }

    @Override
    protected int getTaskCount() {
        return 3;
    }

    @Override
    protected void executeTask(int taskIndex) {
    }

    private String task1execute(String path) {
        boolean created = false;
        File file = new File(path);
        if (!file.exists()) {
            created = file.mkdirs();
        }
        return created ? "Created folder at [" + path + "]" : "Failed";
    }

    private String task2execute(String path) {
        File file = new File(path);
        File folder = file.getParentFile();
        if (!folder.exists()) folder.mkdirs();
        try (PrintWriter writer = new PrintWriter(file, "UTF-8")){
            writer.println("Hello!");
            writer.println("This is text file with some content inside :)");
            return "Created file at [" + path + "]";
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            return "Failed";
        }
    }

    private String task3execute(String path) {
        try {
            JOptionPane.showMessageDialog(null, Files.lines(Paths.get(path)).collect(Collectors.joining("\n")), "Result of reading", JOptionPane.INFORMATION_MESSAGE);
//            return Files.lines(Paths.get(path)).collect(Collectors.joining());
            return "Finished";
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return "Failed";
        }
    }
}
