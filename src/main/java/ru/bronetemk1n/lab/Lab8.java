package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import java.awt.*;

public class Lab8 extends AbstractLabGui {
    @Override
    protected int getTaskCount() {
        return 1;
    }

    @Override
    protected void executeTask(int taskIndex) {
        EventBus.getDefault().post(task1execute(readValues(2)));
    }

    public static String task1execute(Double... args) {
        return String.format("U = %s, Y = %s, K = %s", executeU(args[0], args[1]),
                executeY(args[0], args[1]),
                executeK(args[0], args[1]));
    }

    public static double min(double a, double b) {
        return a < b ? a : b;
    }

    public static double executeU(double a, double b) {
        return min(a, b - a);
    }

    public static double executeY(double a, double b) {
        return min(a * b, a + b);
    }

    public static double executeK(double a, double b) {
        return min(executeU(a, b) + executeY(a, b) * 2, 3.14);
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container result = new Container();
        buildTaskContainerWithInputs(result, "a: ", "b: ");
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        return task1execute(getD(0), getD(1));
    }
}
