package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class Lab1 extends AbstractLabGui {

    public static String task1execute(double x, double y, double z) {
        return String.valueOf(Math.log(Math.abs((y - Math.sqrt(Math.abs(x))) * (x - (y / (z + x * x / 4))))));
    }

    /**
     * a-storona
     * h-visota na storone a
     * <p>
     * b-storona
     * alpha-ugol mezhdu a, b
     * <p>
     * c-storona
     * <p>
     * r-radius vpisannoy okruzhnosti
     * <p>
     * R-radius opisannoy okruzhnosti
     * <p>
     * p-poluperimetr
     */
    public static String task2execute(Map<String, Double> args) {
        if (args.containsKey("a")) {
            if (args.containsKey("h")) {
                return String.valueOf(f1(args.get("a"), args.get("h")));
            }
            if (args.containsKey("b")) {
                if (args.containsKey("alpha")) {
                    return String.valueOf(f2(args.get("a"), args.get("b"), args.get("alpha")));
                }
                if (args.containsKey("c")) {
                    return String.valueOf(f3(args.get("a"), args.get("b"), args.get("c")));
                }
            }
        }
        if (args.containsKey("r") && args.containsKey("p")) {
            return String.valueOf(f4(args.get("r"), args.get("p")));
        }
        return "No formula fit these parameters";
    }

    public static double f1(double a, double h) {
        return a * h * 0.5;
    }

    public static double f2(double a, double b, double alpha) {
        return a * b * Math.sin(alpha) * 0.5;
    }

    public static double f3(double a, double b, double c) {
        double p = semiPerimeter(a, b, c);
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public static double f4(double r, double p) {
        return r * p;
    }

    public static double f5(double R, double a, double b, double c) {
        return (a * b * c) / (4 * R);
    }

    public static double semiPerimeter(double a, double b, double c) {
        return (a + b + c) / 2;
    }

    @Override
    protected int getTaskCount() {
        return 2;
    }

    @Override
    protected void executeTask(int taskIndex) {
        switch (taskIndex) {
            case 1: {
                Double[] values = readValues(3);
                EventBus.getDefault().post(task1execute(values[0], values[1], values[2]));
                break;
            }
            case 2: {
                EventBus.getDefault().post(task2execute(new HashMap<>()));
                break;
            }
        }
    }

    /**
     * a-storona
     * h-visota na storone a
     * <p>
     * b-storona
     * alpha-ugol mezhdu a, b
     * <p>
     * c-storona
     * <p>
     * r-radius vpisannoy okruzhnosti
     * <p>
     * R-radius opisannoy okruzhnosti
     * <p>
     * p-poluperimetr
     */
    @Override
    public Container onContainerBuild(int task) {
        Container result = new Container();
        switch (task) {
            case 1: {
                buildTaskContainerWithInputs(result, "X: ", "Y: ", "Z: ");
                break;
            }
            case 2: {
                buildTaskContainerWithInputs(result, "a: ", "b: ", "c: ", "h: ", "alpha: ", "r: ", "R: ", "p: ");
                break;
            }
        }
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        switch (taskNumber) {
            case 1: {
                return task1execute(getD(0), getD(1), getD(2));
            }
            case 2: {
                Map<String, Double> inputValues = new HashMap<>();
                addIfNotNull(inputValues, "a", getD(0));
                addIfNotNull(inputValues, "b", getD(1));
                addIfNotNull(inputValues, "c", getD(2));
                addIfNotNull(inputValues, "h", getD(3));
                addIfNotNull(inputValues, "alpha", getD(4));
                addIfNotNull(inputValues, "r", getD(5));
                addIfNotNull(inputValues, "R", getD(6));
                addIfNotNull(inputValues, "p", getD(7));
                return task2execute(inputValues);
            }
        }
        return null;
    }

    private void addIfNotNull(Map<String, Double> map, String key, Double input) {
        if (input != null) {
            map.put(key, input);
        }
    }

}
