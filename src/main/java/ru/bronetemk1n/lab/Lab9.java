package ru.bronetemk1n.lab;

import ru.bronetemk1n.lab.gui.AbstractLabGui;
import ru.bronetemk1n.lab.lab9data.Triangle;

import java.awt.*;

public class Lab9 extends AbstractLabGui {

    private Triangle triangle = new Triangle();

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container result = new Container();
        switch (taskNumber) {
            case 1: {
                buildTextContentContainer(result, "Result of the task is in the code");
                break;
            }
            case 2: {
                buildTextContentContainer(result, "Result of the task is in the code");
                break;
            }
            case 3: {
                buildTaskContainerWithInputs(result, "a: ", "b: ", "c: ");
                break;
            }
            case 4: {
                buildTextContentContainer(result, "Result of the task is in the code");
                break;
            }
        }
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        switch (taskNumber) {
            case 3: {
                triangle.setSideA(getD(0));
                triangle.setSideB(getD(1));
                triangle.setSideC(getD(2));
                return String.format("S = %.1f, P = %.1f", triangle.countS(), triangle.countP());
            }
        }
        return "";
    }

    @Override
    protected int getTaskCount() {
        return 4;
    }

    @Override
    protected void executeTask(int taskIndex) {

    }

}
