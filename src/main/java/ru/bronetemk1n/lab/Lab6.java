package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import java.awt.*;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Lab6 extends AbstractLabGui {
    @Override
    protected int getTaskCount() {
        return 2;
    }

    @Override
    protected void executeTask(int taskIndex) {
        switch (taskIndex) {
            case 1: {
                System.out.print("Enter the values count: ");
                EventBus.getDefault().post(task1execute(readValues(scanner.nextInt())));
                break;
            }
        }
    }

    public static String task1execute(Double... srcValues) {
        int minIndex = 0, maxIndex = 0;
        double min = srcValues[0], max = srcValues[0];
        for(int i = 1; i < srcValues.length; i++) {
            if(srcValues[i] < min) {
                minIndex = i;
                min = srcValues[i];
            }
            if(srcValues[i] > max) {
                maxIndex = i;
                max = srcValues[i];
            }
        }
        return String.format("Min: %s, index: %s, \nMax: %s, index: %s", minIndex, min, maxIndex, max);
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container result = new Container();
        switch (taskNumber) {
            case 1: {
                buildTaskContainerWithInputs(result, "values: ");
                break;
            }
//            case 2: {
//                buildTaskContainerWithInputs(result, "n: ");
//                break;
//            }
        }
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        switch (taskNumber) {
            case 1: {
                StringTokenizer input = new StringTokenizer(inputs[0].getText(), ",");
                java.util.List<Double> values = new ArrayList<>();
                while (input.hasMoreTokens()){
                    values.add(Double.valueOf(input.nextToken()));
                }
                Double[] arrValues = new Double[values.size()];
                return task1execute(values.toArray(arrValues));
            }
        }
        return null;
    }
}
