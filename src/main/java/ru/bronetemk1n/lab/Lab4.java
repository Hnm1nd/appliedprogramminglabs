package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import java.awt.*;

public class Lab4 extends AbstractLabGui {


    @Override
    protected int getTaskCount() {
        return 1;
    }

    @Override
    protected void executeTask(int taskIndex) {
        int task = readValues(1)[0].intValue();
        if(task == 3 || task == 5) {
            EventBus.getDefault().post(String.valueOf(task1execute(task, readValues(3))));
        } else {
            EventBus.getDefault().post(String.valueOf(task1execute(task, readValues(2))));
        }
    }

    public static double task1execute(int n, Double[] args) {
        switch (n) {
            case 1: {
                return args[0] * args[1];
            }
            case 2: {
                return args[0] * (args[2] / 2);
            }
            case 3: {
                return (args[0] + args[1]) * (args[2] / 2);
            }
            case 4: {
                return args[3] * (args[4] * args[4]);
            }
            case 5: {
                return args[3] * (args[4] * args[4]) * (args[0] / 360);
            }
        }
        return 0;
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container container = new Container();
        buildTaskContainerWithInputs(container, "N: ", "A: ", "B: ", "H: ", "π: ", "R: ");
        return container;
    }

    @Override
    public String onExecute(int taskNumber) {
        Double[] values = new Double[5];
        for(int i = 0; i < values.length; i++) {
            values[i] = getD(i + 1);
        }
        return String.valueOf(task1execute(Integer.valueOf(inputs[0].getText()), values));
    }
}
