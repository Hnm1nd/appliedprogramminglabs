package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Lab2 extends AbstractLabGui {

    private JTextArea result;

    @Override
    protected int getTaskCount() {
        return 1;
    }

    @Override
    protected void executeTask(int taskIndex) {
        EventBus.getDefault().post(task1execute(readValues(9)));
    }

    public static String task1execute(Double... srcValues) {
        List<Double> values = new ArrayList<>();
        Collections.addAll(values, srcValues);
        Collections.shuffle(values);
        return String.format("%s\n%s %s\n%s\n%s %s\n%s %s %s", values.toArray());
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container container = new Container();
        container.setLayout(new FlowLayout());
        result = new JTextArea("Execution result will be here");
        result.setEditable(false);
        result.setCursor(null);
        result.setOpaque(false);
        result.setFocusable(false);
        result.setFont(UIManager.getFont("Label.font"));
        result.setWrapStyleWord(true);
        result.setLineWrap(true);
        container.add(result);
        return container;
    }

    @Override
    public String onExecute(int taskNumber) {
        result.setText(task1execute(1d, 2d, 3d, 4d, 5d, 6d, 7d, 8d, 9d));
        return "Task finished.";
    }
}
