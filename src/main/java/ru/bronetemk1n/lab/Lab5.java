package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lab5 extends AbstractLabGui {
    @Override
    protected int getTaskCount() {
        return 4;
    }

    @Override
    protected void executeTask(int taskIndex) {
        switch (taskIndex) {
            case 1: {
                EventBus.getDefault().post(String.valueOf(task1execute(readValues(3))));
                break;
            }
            case 2: {
                EventBus.getDefault().post(String.valueOf(task2execute(readValues(1)[0].intValue())));
                break;
            }
            case 3: {
                EventBus.getDefault().post(String.valueOf(task3execute(readValues(1)[0])));
                break;
            }
            case 4: {
                Double[] values = readValues(2);
                EventBus.getDefault().post(String.valueOf(task4execute(values[0], values[1].intValue())));
                break;
            }
        }
    }

    public static double task1execute(Double... srcValues) {
        double min = srcValues[0];
        for(Double value : srcValues) {
            if(value < min) {
                min = value;
            }
        }
        return min;
    }

    public static double task2execute(Integer n) {
        double result = 0;
        for (int i = 1; i <= n; i++) {
            result += 1.0 / (i * i);
        }
        return result;
    }

    public static double task3execute(Double eps) {
        double result = 0;
        for (int i = 1;; i++) {
            int multiple = multiple(i);
            double element = 1 / Math.sqrt(multiple);
            if(element < eps) break;
            result += element;
        }
        return result;
    }

    private static int multiple(int n) {
        int result = 1;
        for(int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    private static int task4execute(double value, int subValue) {
        Pattern pattern = Pattern.compile(String.valueOf(subValue));
        Matcher matcher = pattern.matcher(String.valueOf(value));
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container result = new Container();
        switch (taskNumber) {
            case 1: {
                buildTaskContainerWithInputs(result, "x1: ", "x2: ", "x3: ");
                break;
            }
            case 2: {
                buildTaskContainerWithInputs(result, "n: ");
                break;
            }
            case 3: {
                buildTaskContainerWithInputs(result, "eps: ");
                break;
            }
            case 4: {
                buildTaskContainerWithInputs(result, "number: ", "numeral: ");
                break;
            }
        }
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        switch (taskNumber) {
            case 1: {
                return String.valueOf(task1execute(getD(0), getD(1), getD(2)));
            }
            case 2: {
                return String.valueOf(task2execute(Integer.valueOf(inputs[0].getText())));
            }
            case 3: {
                return String.valueOf(task3execute(getD(0)));
            }
            case 4: {
                return String.valueOf(task4execute(getD(0), Integer.valueOf(inputs[1].getText())));
            }
        }
        return null;
    }
}
