package ru.bronetemk1n.lab.gui;

import org.apache.commons.lang3.math.NumberUtils;
import ru.bronetemk1n.lab.AbstractLab;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractLabGui extends AbstractLab {

    public static final AbstractLabGui EMPTY = new AbstractLabGui() {
        @Override
        public Container onContainerBuild(int taskNumber) {
            Container container = new Container();
            container.setLayout(new FlowLayout());
            container.add(new JLabel("Content is empty"));
            return container;
        }

        @Override
        public String onExecute(int taskNumber) {
            return "";
        }

        @Override
        protected int getTaskCount() {
            return 1;
        }

        @Override
        protected void executeTask(int taskIndex) {
        }
    };
    protected JTextField[] inputs;

    public abstract Container onContainerBuild(int taskNumber);

    public abstract String onExecute(int taskNumber);

    protected void buildTaskContainerWithInputs(Container container, String... labels) {
        int numPairs = labels.length;
        inputs = new JTextField[numPairs];
        container.setLayout(new SpringLayout());

        for (int i = 0; i < labels.length; i++) {
            JLabel l = new JLabel(labels[i], JLabel.TRAILING);
            container.add(l);
            inputs[i] = new JTextField(10);
            l.setLabelFor(inputs[i]);
            container.add(inputs[i]);
        }

        SpringUtilities.makeCompactGrid(container,
                numPairs, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
    }

    protected void buildTextContentContainer(Container container, String text) {
        container.setLayout(new FlowLayout());
        JTextArea resultArea = new JTextArea(text);
        resultArea.setEditable(false);
        resultArea.setCursor(null);
        resultArea.setOpaque(false);
        resultArea.setFocusable(false);
        resultArea.setFont(UIManager.getFont("Label.font"));
        resultArea.setWrapStyleWord(true);
        resultArea.setLineWrap(true);
        container.add(resultArea);
    }

    protected Container buildTaskContainerWithInputs(String... labels) {
        Container result = new Container();
        buildTaskContainerWithInputs(result, labels);
        return result;
    }

    protected Container buildTextContentContainer(String text) {
        Container result = new Container();
        buildTextContentContainer(result, text);
        return result;
    }

    protected Double getD(int inputIndex) {
        String inputText = inputs[inputIndex].getText();
        if (inputText == null || inputText.isEmpty() || !NumberUtils.isCreatable(inputText)) {
            return null;
        }
        return Double.valueOf(inputs[inputIndex].getText());
    }
}
