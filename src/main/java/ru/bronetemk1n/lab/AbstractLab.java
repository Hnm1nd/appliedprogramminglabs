package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class AbstractLab implements Runnable {

    public static final AbstractLab EMPTY = new AbstractLab() {
        @Override
        protected int getTaskCount() {
            return 0;
        }

        @Override
        protected void executeTask(int taskIndex) {}
    };

    protected Scanner scanner = new Scanner(System.in);

    @Override
    public void run() {
        int taskCount = getTaskCount();
        if(taskCount > 1) {
            System.out.print(String.format("Choose task from %s tasks: ", taskCount));
            executeLabTask(scanner.nextInt());
        } else {
            executeLabTask(1);
        }
    }

    public Double[] readValues(int count) {
        List<Double> result = new ArrayList<>();
        System.out.println("Write " + count + " values: ");
        for (int i = 0; i < count; i++) {
            result.add(scanner.nextDouble());
        }
        Double[] resultArr = new Double[result.size()];
        return result.toArray(resultArr);
    }

    public String readString() {
        System.out.println("Write some string: ");
        return scanner.nextLine();
    }

    public String readString(String input) {
        System.out.println(input);
        return scanner.nextLine();
    }

    private void executeLabTask(int taskIndex) {
        if(taskIndex < 1 || taskIndex > getTaskCount()) {
            EventBus.getDefault().post(String.format("There is no task number %s", taskIndex));
        } else {
            executeTask(taskIndex);
        }
    }

    public void sendResult(String result) {
        EventBus.getDefault().post(result);
    }

    protected abstract int getTaskCount();

    protected abstract void executeTask(int taskIndex);

}
