package ru.bronetemk1n.lab;

import org.greenrobot.eventbus.EventBus;
import ru.bronetemk1n.lab.gui.AbstractLabGui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lab7 extends AbstractLabGui {

    private JTextArea resultArea;

    @Override
    protected int getTaskCount() {
        return 3;
    }

    @Override
    protected void executeTask(int taskIndex) {
        switch (taskIndex) {
            case 1: {
                EventBus.getDefault().post(task1execute(readString()));
                break;
            }
            case 2: {
                EventBus.getDefault().post(task2execute(readString(), readString()));
                break;
            }
            case 3: {
                String src = readString("Enter the elements of list: ");
                List<String> values = new ArrayList<>();
                StringTokenizer tokenizer = new StringTokenizer(src, " ");
                while (tokenizer.hasMoreTokens()) {
                    values.add(tokenizer.nextToken());
                }
                EventBus.getDefault().post(task3execute(values));
                break;
            }
        }
    }

    public static String task1execute(String input) {
        int aCount = subStringCount(input, "a"),
                bCount = subStringCount(input, "b");
        if(aCount > bCount) {
            return String.format("a (%s) > b (%s)", aCount, bCount);
        } else {
            return String.format("a (%s) < b (%s)", aCount, bCount);
        }
    }

    public static int subStringCount(String input, String subString) {
        Pattern pattern = Pattern.compile(subString);
        Matcher matcher = pattern.matcher(input);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    public static String task2execute(String input, String substring){
        return input.replaceAll(substring, "");
    }

    public static List<String> task3execute(List<String> src) {
        List<String> result = new ArrayList<>(src);
        Collections.reverse(result);
        return result;
    }

    @Override
    public Container onContainerBuild(int taskNumber) {
        Container result = new Container();
        switch (taskNumber) {
            case 1: {
                buildTaskContainerWithInputs(result, "Input string: ");
                break;
            }
            case 2: {
                buildTaskContainerWithInputs(result, "Input string: ", "Substring: ");
                break;
            }
            case 3: {
                Container container = new Container();
                container.setLayout(new FlowLayout());
                buildTaskContainerWithInputs(result, "Input");
                resultArea = new JTextArea("Execution result will be here");
                resultArea.setEditable(false);
                resultArea.setCursor(null);
                resultArea.setOpaque(false);
                resultArea.setFocusable(false);
                resultArea.setFont(UIManager.getFont("Label.font"));
                resultArea.setWrapStyleWord(true);
                resultArea.setLineWrap(true);
                container.add(result);
                container.add(resultArea);
                return container;
            }
        }
        return result;
    }

    @Override
    public String onExecute(int taskNumber) {
        switch (taskNumber) {
            case 1: {
                return task1execute(inputs[0].getText());
            }
            case 2: {
                return task2execute(inputs[0].getText(), inputs[1].getText());
            }
            case 3: {
                StringTokenizer input = new StringTokenizer(inputs[0].getText(), ",");
                java.util.List<String> values = new ArrayList<>();
                while (input.hasMoreTokens()){
                    values.add(input.nextToken());
                }
                java.util.List<String> resultList = task3execute(values);
                StringBuilder result = new StringBuilder();
                for(String value:resultList) {
                    result.append(value);
                    result.append("\n");
                }
                resultArea.setText(result.toString());
                return "Task finished.";
            }
        }
        return null;
    }
}
