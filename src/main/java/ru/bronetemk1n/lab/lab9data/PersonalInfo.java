package ru.bronetemk1n.lab.lab9data;

import java.util.Map;

public class PersonalInfo {

    private Map<String, String> passportData;

    public PersonalInfo(Map<String, String> somePassportData) {
        this.passportData = somePassportData;
    }

    public Map<String, String> getSomePassportData() {
        return passportData;
    }

    public void setSomePassportData(Map<String, String> somePassportData) {
        this.passportData = somePassportData;
    }
}
