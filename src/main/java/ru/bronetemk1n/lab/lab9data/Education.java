package ru.bronetemk1n.lab.lab9data;

import java.util.List;

public class Education {

    private int level;
    private String[] exams;
    private String speciality;

    public Education(int level, String[] exams, String speciality) {
        this.level = level;
        this.exams = exams;
        this.speciality = speciality;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String[] getExams() {
        return exams;
    }

    public void setExams(String[] exams) {
        this.exams = exams;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
}
