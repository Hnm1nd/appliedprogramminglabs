package ru.bronetemk1n.lab.lab9data;

import ru.bronetemk1n.lab.Lab1;

public class Triangle {

    private double angleAB,
            angleAC,
            angleBC;

    private double sideA,
            sideB,
            sideC;

    public Triangle() {
        setSideA(0);
        setSideB(0);
        setSideC(0);
        setAngleAB(60);
        setAngleBC(60);
        setAngleAC(60);
    }

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double getAngleAB() {
        return angleAB;
    }

    public void setAngleAB(double angleAB) {
        this.angleAB = angleAB;
    }

    public double getAngleAC() {
        return angleAC;
    }

    public void setAngleAC(double angleAC) {
        this.angleAC = angleAC;
    }

    public double getAngleBC() {
        return angleBC;
    }

    public void setAngleBC(double angleBC) {
        this.angleBC = angleBC;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    public double countS() {
        return Lab1.f3(sideA, sideB, sideC);
    }

    public double countP() {
        return sideA + sideB + sideC;
    }
}
